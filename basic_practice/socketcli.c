#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define PATH "/sys/bus/w1/devices/"
#define SERVER_IP   "49.234.23.17"
//#define SERVER_IP   "127.0.0.1"
#define SERVER_PORT 8889
int get_temperature(char *buffer);
int main(int argc,char **argv)
{
	int con_fd = -1;
	int rv = -1;
	char buffer[1024];
	struct  sockaddr_in serv_addr;
	con_fd = socket(AF_INET,SOCK_STREAM,0);

	if(con_fd < 0)

	{

		printf("Failed to create con_fd: %s\n",strerror(errno));
		return -1;

	}

	memset(&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(SERVER_PORT);
	inet_aton(SERVER_IP,&serv_addr.sin_addr.s_addr);
	if(connect(con_fd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
	{
		printf("Failed to connect server: %s\n",strerror(errno));
		return -2;
	}
while(1)
{
    memset(buffer,0,sizeof(buffer));
	get_temperature(buffer);

	if( (rv = write(con_fd,buffer,sizeof(buffer))) < 0)
	{
		printf("Failed to write into con_fd；%s\n",strerror(errno));
		goto end;
	}

	if(rv == 0)
	{
		printf("Disconnet from the server.\n");
		goto end;
	}

    sleep(10);
}
end:
	close(con_fd);
	return 0;
}
int get_temperature(char *buffer)
{
	DIR             *dirp;
	struct dirent   *direntp;
	char            path[1024];
	char            tempbuffer[1024];
	int             fd;
	int             rv;
	char            *result;
	float           t;

	if((dirp = opendir(PATH)) == NULL)
	{
		printf("Failed to open the destination directory: %s\n",strerror(errno));
		return -1;
	}

	while((direntp = readdir(dirp)) != NULL)
	{
		if(strstr(direntp->d_name,"28-") != NULL)
		{
			break;
		}
	}

	if(direntp == NULL)
	{
		printf("Failed to find the destination directory: 28-*** .\n");
		return -2;
	}
	memset(path,0,sizeof(path));
	strcat(path,PATH);
	strcat(path,direntp->d_name);
	strcat(path,"/w1_slave");
	closedir(dirp);
	if((fd = open(path,O_RDONLY)) < 0)

	{
		printf("Failed to open the destination file.\n");
		return -3;

	}
	memset(tempbuffer,0,sizeof(tempbuffer));
	while((rv = read(fd,tempbuffer,sizeof(tempbuffer))) > 0)
	{                                                                                           
		if((result = strstr(tempbuffer,"t=")) != NULL)
		{
			result = result + 2;
			break;
		}
	}

	if(rv <= 0)
	{
		printf("Failed to read the temperature.\n");
		return -4;

	}
	t = atof(result)/1000;
	sprintf(buffer,"%f",t);
	return 0;
}
