#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#define STR "hello world!"
int main (int argc,int *argv[])
{
	int fd = -1;
	int rv = -1;
	char buffer[1024];
	fd = open("hello.txt",O_RDWR|O_CREAT|O_TRUNC,0666);
	if(fd < 0)
	{
		printf("Open or create the file 'hello.txt' failure.\n");
		goto end;
	}
	else
		printf("Open the file successfully. the return value is [%d]\n",fd);
	rv = write(fd,STR,strlen(STR));
	lseek(fd,0,SEEK_SET);
	if(rv < 0)
	{
		printf("Failed to write data into the file: %s\n",strerror(errno));
		goto end;
	}
	else
		printf("Succeed to write data into the file.\n");
		printf("Now start to read data from the file.\n");
	memset(buffer,0,sizeof(buffer));
	rv = read(fd,buffer,sizeof(buffer));
	lseek(fd,0,SEEK_SET);
	if(rv < 0)
	{
		printf("failed to read data from the file: %s\n",strerror(errno));
		goto end;
	}
	else
		printf("Succeed to read %d bytes data form the file: %s\n",rv,buffer);

end:
	close(fd);

	return 0;
}
