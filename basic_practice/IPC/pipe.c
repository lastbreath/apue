/*********************************************************************************
 *      Copyright:  (C) 2021 lastbreath<2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  pipe.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(10/28/2021)
 *         Author:  lastbreath <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "10/28/2021 05:13:35 PM"
 *                 
 ********************************************************************************/
                                                                                    

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MSG_STR "This message is from parent: Hello,child process!"

int main (int argc, char **argv)
{
    int         pipe_fd[2];
    int         pid;
    char        buffer[512];
    int         rv;

    rv = pipe(pipe_fd);

    if(rv < 0)
    {
        printf("%s faild: %s\n",__func__,strerror(errno));
        return -1;
    }

    pid = fork();
    printf("%d\n",pid);
    if(pid < 0)
    {
        printf("%s faild: %s\n",__func__,strerror(errno));
        return -1;
    }

    else if(pid == 0)
    {
          
        printf("50\n");
            close(pipe_fd[1]);

          memset(buffer,0,sizeof(buffer));

          rv = read(pipe_fd[0],buffer,sizeof(buffer));

          if(rv < 0)
          {
              printf("child process read from pipe faild: %s\n",strerror(errno));
              return -1;
          }

          else if(rv == 0)
          {
              printf("parent process has cut down the link.\n");
              return -1;
          }

          printf("child process read %d bytes data from pipe :[%s]\n",rv,buffer);
    }

    else if(pid > 0)
    {
        close(pipe_fd[1]);
        printf("73\n");

        /*  if((write(pipe_fd[1],MSG_STR,strlen(MSG_STR))) < 0)
        {
             printf("Parent process write data to pipe failure: %s\n",strerror(errno));
             return -1;
        }*/
        printf("Parent start wait child process exit...\n");
        while(1)
        
        {


        }

        printf("1\n");
        wait(NULL);
    }

    return 0;
} 

