/*********************************************************************************
 *      Copyright:  (C) 2021 lastbreath<2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  signal.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(10/28/2021)
 *         Author:  lastbreath <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "10/28/2021 03:15:04 PM"
 *                 
 ********************************************************************************/


#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>


int g_child_stop    = 0;
int g_parent_stop   = 0;


void sighandler_child(int num)
{
        g_child_stop    = 1;
}


void sighandler_parent(int num)
{
    g_parent_stop       =  1;
}


int main (int argc, char **argv)
{
    int pid;


    signal(SIGUSR1,sighandler_child);
    signal(SIGUSR2,sighandler_parent);
    

    printf("create a process.\n");
    pid = fork();

    if(pid < 0)
    {
        printf("%s failed: %s\n",__func__,strerror(errno));
        return -1;
    }
    if(pid > 0)
    {

        printf("now in parent process: [%d].\n",getpid());
        printf("process :[%d] hangs up and let child process running first.\n",getpid());
          while(!g_parent_stop)
        {
            sleep(1);
        } 

        kill(pid,SIGUSR1);
        int x = wait(NULL);
        printf("%d\n",x);
        printf("parent process: [%d] concluded.\n",getpid());
    }

  
    if(0 == pid)//get in child process
    {
        printf("now in child process: [%d].\n",getpid());
        printf("process: [%d] send a signal to parent process to notice that his work has done.\n ",getpid());
         
       kill(getppid(),SIGUSR2);
       while(!g_child_stop)
        {
          sleep(1);
        }
        printf("child process receive parent's signal and concludes.\n");
    }
  

    return 0;
} 
 
