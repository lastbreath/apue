#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <getopt.h>

int main (int argc,char *argv[])
{
	int	ch;
	char	*srcfile;
	char	*destfile;
	int	fd1;
	int	fd2;
	int	rv1;
	int	rv2;
	char	buffer[1024];
	struct option opts[] = 
	{
		{"source",required_argument,NULL,'s'},
		{"destination",required_argument,NULL,'d'},
		{"help",no_argument,NULL,'h'},
		{NULL,0,NULL,0,NULL}
	};
	while((ch = getopt_long(argc,argv,"s:d:h",opts,NULL)) != -1)
	{
		switch(ch)
		{
			case 's':
				srcfile = optarg;
				break;
			case 'd':
				destfile = optarg;
				break;
			case 'h':
				print_usage(argv[0]);
				return 0;
		}
	}

	fd1 = open(srcfile,O_RDONLY);
	if(fd1 < 0)
	{
		printf("Failed to open the source file.\n");
		print_usage(argv[0]);
		return 0;
	}

	fd2 = open(destfile,O_RDWR|O_CREAT|O_TRUNC,0666);
	if(fd2 < 0)
	{
		printf("Failed to open the destination file.\n");
		return 0;
	}
	memset(buffer,0,sizeof(buffer));
	while((rv1 = read(fd1,buffer,sizeof(buffer))) > 0)
	{
		rv2 = write(fd2,buffer,rv1);
		if(rv2 < 0)
		{
			printf("Failed to write data into destination file: %s\n",strerror(errno));
			goto end;
		}
	}


end:
	close(fd1);
	close(fd2);


	return 0;

}
void print_usage(char *progname)
{
	printf("%s usage: \n",progname);
	printf("example: ./mycp file1 file2\nexplain: file1 must has been exist!\nThere are no request on file2.\n");
	printf("-s(--source)sepcify the source file(must existed!).\n");
	printf("-d(--destination)specify the destination file.\n");
	printf("-h(--hlep): print this hlep information.\n");
}


