#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <mysql.h>
#define ARRAY_SIZE(x)  (sizeof(x)/sizeof(x)[0])
typedef void *(*THREAD_BODY) (void *);
void print_usage(char *progname);
int fileio (char *buffer);
int socket_server_init(char *listen_ip,int listen_port);
int dboperate(float t);

int main(int argc,char **argv)
{ 
     	struct option options[] =        
	{

		{"daemon",no_argument,NULL,'d'},        
		{"port",required_argument,NULL,'p'},
		{"help",no_argument,NULL,'h'},
		{NULL,0,NULL,0}

	};

	int         listen_fd;
	int         cli_fd;
	int         port = 0;
	int         is_daemon = 0;
	int         opt;
	fd_set      read_set;
	char        buffer[1024];
	int         fds_array[1024];
	int         maxfd = 0;
	struct sockaddr_in cli_addr;
	socklen_t   cli_addr_len;
	int         found;
	int         rv;
	while((opt = getopt_long(argc,argv,"dhp:",options,NULL)) != -1)
	{
		switch(opt)
		{
			case 'd':
				is_daemon = 1;
				break;
			case 'p':
				port = atoi(optarg);
				break;
			case 'h':
				print_usage(argv[0]);
				return 0;
			default:
				break;
		}
	}

	if(!port)
	{
		print_usage(argv[0]);
		return -1;
	}
	if(is_daemon)
	{
		daemon(0,0);
	}

	listen_fd = socket_server_init(NULL,port);
	if(listen_fd < 0)
	{
		printf("Failed to excute socket_server_init(): %s.\n",strerror(errno));
		return -2;

	}

	for(int i = 0;i<ARRAY_SIZE(fds_array);i++)
	{
		fds_array[i] = -1;
	}

	fds_array[0] = listen_fd;
	for( ; ; )
	{
		FD_ZERO(&read_set);
		for(int i = 0;i<ARRAY_SIZE(fds_array);i++)
		{
			if(fds_array[i] < 0)
			{
				continue;  
			}
			maxfd = fds_array[i] > maxfd ? fds_array[i] : maxfd;
			FD_SET(fds_array[i],&read_set);
		}
		rv = select(maxfd+1,&read_set,NULL,NULL,NULL);
		if(rv < 0)
		{
			printf("Failed to select: %s.\n",strerror(errno));
			break;

		}

		if(FD_ISSET(listen_fd,&read_set))
		{
			if((cli_fd = accept(listen_fd,(struct sockaddr *)&cli_addr,&cli_addr_len)) < 0)
			{
				printf("Failed to accept cli_fd: %s.\n",strerror(errno));
				continue;
			}

			found = 0;

			for(int i = 0;i < ARRAY_SIZE(fds_array);i++)
			{
				if(fds_array[i] < 0)
				{
					printf("Accept new client[%d] and add it into array\n",cli_fd);
					fds_array[i] = cli_fd;
					found = 1;
					break;
				}
			}

			if(!found)
			{
				printf("Accept new client[%d] but full,so refuse it.\n",cli_fd);
				close(cli_fd);
			}
		}
		else
		{
			for(int i = 0;i < ARRAY_SIZE(fds_array);i++)
			{
				if(fds_array[i] < 0 || !FD_ISSET(fds_array[i],&read_set))
				{
				
					continue; 
				}				
				memset(buffer,0,sizeof(buffer));

				if((rv = read(fds_array[i],buffer,sizeof(buffer))) <= 0)
				{
					printf("Failed to read data from client_fd: %s\n",strerror(errno));
					close(fds_array[i]);
					fds_array[i] = -1;
				}
				else
				{

					dboperate(atof(buffer));
					//fileio(buffer);
					close(fds_array[i]);
					fds_array[i] = -1;
				}
			}
		}
	}
end:
	close(listen_fd);
	return 0;
}

int socket_server_init(char *listen_ip,int listen_port)
{
	struct sockaddr_in  serv_aadr;
	int on = 1;
	int rv = 0;
	int listen_fd;
	if((listen_fd = socket(AF_INET,SOCK_STREAM,0)) < 0)
	{
		printf("Failed to create listen_fd: %s\n",strerror(errno));
		return -1;
	}
	setsockopt(listen_fd,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));
	memset(&serv_aadr,0,sizeof(serv_aadr));
	serv_aadr.sin_family = AF_INET;
	serv_aadr.sin_port = htons(listen_port);
	if(!listen_ip)
	{
		serv_aadr.sin_addr.s_addr = htonl(INADDR_ANY);
	}
	else
	{
		if(inet_pton(AF_INET,listen_ip,&serv_aadr.sin_addr) <= 0 )
		{   
			printf("Failed to use inet_pton() to set listen IP address.\n");
			rv = -2;
			goto end;
		}
	}
	if(bind(listen_fd,(struct sockaddr *)&serv_aadr,sizeof(serv_aadr)) < 0)
	{
		printf("Failed to bind the listen_fd: %s.\n",strerror(errno));
		rv = -3;
		goto end;
	}
	if(listen(listen_fd,13) < 0)
	{
		printf("Failed to start listen listen_fd: %s.\n",strerror(errno));
		rv = -4;
		goto end;
	}
end:
	if(rv < 0)
		close(listen_fd);

	else
		rv = listen_fd;
	return rv;
}

void print_usage(char *progname)
{
	printf("%s usage:\n",progname);
	printf("-p(--port): sepcify server listen port.\n");
	printf("-h(--Help): print this help information.\n");
	printf("-d(--daemon): set program running on background.\n");
	printf("\nExample: %s -d -p 8889\n",progname);
}

int fileio (char *buffer)
{
	int fd = -1;
	int rv = -1;
	fd = open("log.txt",O_RDWR|O_CREAT|O_APPEND,0666);
	if(fd < 0)
	{
		printf("Open or create the file 'log.txt' failure.\n");
		goto end;
	}
	rv = write(fd,buffer,1024);//question!!!
	if(rv < 0)
	{
		printf("Failed to write data into the file: %s\n",strerror(errno));
		goto end;
	}
end:                     
	close(fd);
	return 0;
}

int dboperate(float t)
{
	MYSQL mysql,*sock;      
	MYSQL_RES *res;
	MYSQL_FIELD *fd;
	MYSQL_ROW row;
	char qbuf[160];
	mysql_init(&mysql);
	
	sock = mysql_real_connect(&mysql,"localhost","root","12345678","mydb",0,NULL,0);
	
	if(!sock)						
	{
		fprintf(stderr,"Couldn't connect to database!: %s\n",mysql_error(&mysql));
		exit(1);
	}               
	
	sprintf(qbuf,"insert temperature(subtime,temperature) values(now(),%f)",t);
	
	if(mysql_query(sock,qbuf))// when mysql_query() excute seccessfully it return 0;
	{
		fprintf(stderr,"Excute failed!: %s\n",mysql_error(sock));
		exit(1);
	}
	
	return 0;
}
