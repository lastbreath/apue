/*********************************************************************************
 *      Copyright:  (C) 2021 lastbreath<2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  socketserv_epoll.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(08/08/2021)
 *         Author:
 *
 *         lastbreath <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "08/08/2021 05:08:41 PM"
 *                 
 ********************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <mysql.h>

#define MAX_EVENTS  512
#define ARRY_SIZE(x)    (sizeof(x)/sizeof(x[0]))


static inline void print_usage(char *progname);
int socket_server_init(char *listen_ip,int listen_port);
void set_socket_rlimt(void);
int fileio (char *buffer);
int dboperate(float t);

int main (int argc, char **argv)
{
    int listen_fd;
    int client_fd;
    int serv_port;
    int daemon_run = 0;
    int opt;
    int rv;
    int found;
    char buffer[1024];

    int epollfd;
    struct epoll_event  event;
    struct epoll_event event_array[MAX_EVENTS];
    int nepfd;


     struct option options[] = 
     {
         {"daemon",no_argument,NULL,'d'},   
         {"port",required_argument,NULL,'p'},
         {"help",no_argument,NULL,'h'},
         {NULL,0,NULL,0}
     };

     while((opt = getopt_long(argc,argv,"dhp:",options,NULL)) != -1)
     {
        
         switch(opt)
         
         {
             case 'd':
                 daemon_run = 1;
                 break;
             case 'p':
                 serv_port = atoi(optarg);
                 break;
             case 'h':
                 print_usage(argv[0]);
                 return 0;
             default:
                 break;
         }
     }

     if(!serv_port)
     {
         print_usage(argv[0]);
         return -1;
     }

    
     if(daemon_run)
     {
         daemon(0,0);
     }
     
        
     //printf("98\n");
     listen_fd = socket_server_init(NULL,serv_port);
     
     if(listen_fd < 0)
     {
         printf("Failed to excute socket_server_init(): %s.\n",strerror(errno));
         return -2;
     }

     //printf("99\n");
     set_socket_rlimt();

    // printf("100\n");
     epollfd = epoll_create(MAX_EVENTS);

     //printf("101\n");
     if(epollfd < 0)   
     {
        printf("epoll_create() failure: %s\n", strerror(errno));
        //return -3;
        goto end;
     }

     //printf("102\n");
     event.events = EPOLLIN;
     event.data.fd = listen_fd;

     if(epoll_ctl(epollfd,EPOLL_CTL_ADD,listen_fd,&event) < 0)
     {
         printf("epoll add listen socket failure: %s\n",strerror(errno));
         //return -4;
         goto end;
     }


     //printf("103\n");
     for( ; ; )
     {
         nepfd = epoll_wait(epollfd,event_array,MAX_EVENTS,-1);

         if(nepfd < 0)
         {
             printf("epoll_wait() failure: %s\n",strerror(errno));
             break;
         }

         for(int i = 0; i < nepfd ; i++ )
         {
             if((event_array[i].events & EPOLLERR) || (event_array[i].events & EPOLLHUP))
             {
                 printf("epoll_wait get error on fd[%d]: %s\n",event_array[i].data.fd,strerror(errno));
                 epoll_ctl(epollfd,EPOLL_CTL_DEL,event_array[i].data.fd,NULL);
                 close(event_array[i].data.fd);
             }
             if(event_array[i].data.fd == listen_fd)
             {
                 client_fd = accept(listen_fd,(struct sockaddr *)NULL,NULL);
                 if(client_fd < 0)
                 {
                     printf("Failed to accept a new clientfd: %s\n",strerror(errno));
		     //return;
		     //goto end;
                     continue;
                 }

                 event.data.fd = client_fd;
                 event.events = EPOLLIN;

                 if(epoll_ctl(epollfd,EPOLL_CTL_ADD,client_fd,&event) < 0)
                 {
                     printf("epoll add client socket failure: %s\n", strerror(errno));
                     close(event_array[i].data.fd);
                     continue;
                 }
	     }
                 else
                 {
                     memset(buffer,0,sizeof(buffer));
                     if((rv = read(event_array[i].data.fd,buffer,sizeof(buffer))) <= 0)
                     {
                        printf("Failed to read data from client_fd: %s\n",strerror(errno));
                        epoll_ctl(epollfd,EPOLL_CTL_DEL,event_array[i].data.fd,NULL);
                        close(event_array[i].data.fd);
			//printf("error\n");
			continue;
                     }
                     else
                     {
                         //fileio(buffer);
			 printf(buffer);
			 //printf("\n");
			 dboperate(atof(buffer));
                     }
                 }

             }
         }
     

    

end:
     close(listen_fd);
     return 0;
} 

void print_usage(char *progname)
{
    printf("%s usage:\n",progname);
    printf("-p(--port): sepcify server listen port.\n");
    printf("-h(--Help): print this help information.\n");
    printf("-d(--daemon): set program running on background.\n");
    printf("\nExample: %s -d -p 8889\n",progname);
}

int fileio(char *buffer)
{
    int fd = -1;
    int rv = -1;

    fd =open("log.txt",O_RDWR|O_CREAT|O_APPEND,0666);
    if(fd < 0)
    {
        printf("Open or create the file 'log.txt' failure.\n");
        goto end;
    }

    rv = write(fd,buffer,1024);
        if(rv < 0)
        {
            printf("Failed to write data into the file: %s\n",strerror(errno));
            goto end;
        }

end:                     
        close(fd);
        return 0;
}

void set_socket_rlimt(void)
{
    struct rlimit limit;
    getrlimit(RLIMIT_NOFILE, &limit );
    limit.rlim_cur = limit.rlim_max;
    setrlimit(RLIMIT_NOFILE, &limit );

}

int socket_server_init(char *listen_ip,int listen_port)
{
    struct  sockaddr_in serv_addr;
    int on = 1;
    int rv = 0;
    int listen_fd;

    //setsockopt(listen_fd,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));
    if((listen_fd = socket(AF_INET,SOCK_STREAM,0)) < 0)
    {
        printf("Failed to create listen_fd: %s\n",strerror(errno));
        return -1;
    }

    setsockopt(listen_fd,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));

    memset(&serv_addr,0,sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(listen_port);
    if(!listen_ip)
    {
        serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    }
    else
    {
        if(inet_pton(AF_INET,listen_ip,&serv_addr.sin_addr) <= 0)
        {
            printf("Failed to use inet_pton() to set listen IP address.\n");
            rv = -2;
            goto end;
        }
    }

    if(bind(listen_fd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
    {
        printf("Failed to bind the listen_fd: %s.\n",strerror(errno));
        rv = -3;
        goto end;
    }

    if(listen(listen_fd,13) < 0)
    {
        printf("Failed to start listen listen_fd: %s.\n",strerror(errno));
        rv = -4;
        goto end;
    }

end:
    if(rv < 0)
        close(listen_fd);
    else
        rv = listen_fd;
    return rv;

}


int dboperate(float t)
{
	        MYSQL mysql,*sock;	
		MYSQL_RES *res;
		MYSQL_FIELD *fd;
		MYSQL_ROW row;

		char qbuf[160];

		mysql_init(&mysql);

	       	sock = mysql_real_connect(&mysql,"localhost","root","12345678","mydb",0,NULL,0);
		if(!sock)
		{
			fprintf(stderr,"Couldn't connect to database!: %s\n",mysql_error(&mysql));
			exit(1);
		}		
		sprintf(qbuf,"insert temperature(subtime,temperature) values(now(),%f)",t);

		if(mysql_query(sock,qbuf))// when mysql_query() excute seccessfully it return 0;

		{
			fprintf(stderr,"Excute failed!: %s\n",mysql_error(sock));
			exit(1);
		}
		return 0;
}


