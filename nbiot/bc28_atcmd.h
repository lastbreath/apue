/********************************************************************************
 *      Copyright:  (C) 2021 chenyujiang <2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  bc28_atcmd.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(11/12/21)
 *         Author:  chenyujiang <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "11/12/21 12:13:58"
 *                 
 ********************************************************************************/


#ifndef __BC28_ATCMD_H__
#define __BC28_ATCMD_H__

#include "atcmd.h"

#define MAX_SIZE 256

#if 0
enum
{
    
}
#endif

extern int atcmd_at(t_comport *comport);

extern int atcmd_cimi(t_comport *comport,char *cimi,int size);
extern int atcmd_csq(t_comport *comport);
extern int atcmd_nband(t_comport *comport);
extern int atcmd_cgatt(t_comport *comport);
extern int atcmd_cereg(t_comport *comport);
extern int atcmd_cgsn(t_comport *comport,char *imei,int size);
extern int atcmd_cgmm(t_comport *comport,char *name,int size);
extern int atcmd_cgmr(t_comport *comport,char *version,int size);
extern int atcmd_cgpaddr(t_comport *comport);
extern int atcmd_ncdp(t_comport *comport,char *ip,char *port);
#endif

