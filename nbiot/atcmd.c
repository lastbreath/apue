/*********************************************************************************
 *      Copyright:  (C) 2021 chenyujiang <2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  atcmd.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(11/12/21)
 *         Author:  chenyujiang <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "11/12/21 07:58:25"
 *                 
 ********************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>


#include "atcmd.h"

int send_atcmd(t_comport *comport,char *atcmd,char *buffer,int buffer_size,int     timeout)
{

    if(!comport || !atcmd || !buffer || buffer_size <= 0)
    {
        
        printf ("%s: invalid_argument\n",__func__);
        return -1;
    }

    int                 rv = -1;
    fd_set              rfds;
    struct  timeval     time_out;
    struct  timeval     time;
    
    rv = comport_write(comport,atcmd,strlen(atcmd));

    if(rv < 0)
    {
        printf("%s: comport_write() failed\n",__func__);
        return rv;
    }
    //usleep(10000);
    memset(buffer,0,sizeof(*buffer));
    rv = comport_read(comport,buffer,buffer_size,300);

    if(rv< 0)
    {
        printf("%s: comport_read() failed\n",__func__);
        return rv;
    }

    return 0;

}


int at_fetch(char *at_reply,char *start_key,char *end_key,char  *buffer,int        buffer_size)
{
    if(!at_reply || !start_key || buffer_size <= 0)
    {

        printf ("%s: invalid_argument\n",__func__);
        return -1;
    }

    char        *start_buffer;
    char        *end_buffer;

    memset(buffer,0,sizeof(*buffer));

    start_buffer = strstr(at_reply,start_key);

    if(!start_buffer)
    {
        printf("strstr start_key: %s failed\n",start_key);
        return -1;
    }

    if(!end_key)
    {
        return (start_buffer - at_reply);
    }

    start_buffer += strlen(start_key);

    end_buffer = strstr(start_buffer,end_key);
    
    if(!buffer)
    {  
        if(!end_buffer)
        {
            printf("strstr start_key: %s failed\n",end_key);
            return -1;
        }

        return (end_buffer-start_buffer);
    }
    if(buffer)
    {
        if((end_buffer-start_buffer) > buffer_size)
        {
            printf("the specify buffer is not enough\n");
            return -1;
        }

        strncpy(buffer,start_buffer,end_buffer - start_buffer);
    }

    return 0;

}
