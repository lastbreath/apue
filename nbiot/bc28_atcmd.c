/*********************************************************************************
 *      Copyright:  (C) 2021 chenyujiang <2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  bc28_atcmd.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(11/12/21)
 *         Author:  chenyujiang <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "11/12/21 12:21:34"
 *                 
 ********************************************************************************/

#ifndef BUFFER_SIZE
#define BUFFER_SIZE 512
#endif


#define TIMEOUT     50


#include "bc28_atcmd.h"
#include "logger.h"


int atcmd_at(t_comport *comport)
{

    if(!comport)
    {
        
        log_err("%s: invalid_argument\n",__func__);
        return -1;
    }

    char            send_buffer[BUFFER_SIZE];
    char            receive_buffer[BUFFER_SIZE];
    int             rv;

    memset(send_buffer,0,sizeof(send_buffer));
    memset(receive_buffer,0,sizeof(receive_buffer));

    rv = send_atcmd(comport,"at\r\n",send_buffer,sizeof(send_buffer),TIMEOUT);
    if(rv  < 0)
    {
        log_err("%s: send_atcmd() failed\n",__func__);
        return rv;
    }
    
    rv  = at_fetch(send_buffer,"\r\n","\r\n",receive_buffer,sizeof(receive_buffer));
    if(rv < 0)
    {
        log_err("%s: at_fetch() failed\n",__func__);
        return rv;
    }
    
    if(strncmp(receive_buffer,"OK",2) != 0)
    {
        log_err("NBIOT is not connected..\n");
        return -1;
    }
    log_nrml("AT:%s\n",receive_buffer);
    return 0;

}

int atcmd_csq(t_comport *comport)
{

    if(!comport)
    {
        
        log_err("%s: invalid_argument\n",__func__);
        return -1;
    }

    char            send_buffer[BUFFER_SIZE];
    char            receive_buffer[BUFFER_SIZE];
    int             rv;

    memset(send_buffer,0,sizeof(send_buffer));
    memset(receive_buffer,0,sizeof(receive_buffer));

    rv = send_atcmd(comport,"at+csq\r\n",send_buffer,sizeof(send_buffer),TIMEOUT);
    if(rv  < 0)
    {
        log_err("%s: send_atcmd() failed\n",__func__);
        return rv;
    }
    
    rv  = at_fetch(send_buffer,":",",",receive_buffer,sizeof(receive_buffer));
    if(rv < 0)
    {
        log_err("%s: at_fetch() failed\n",__func__);
        return rv;
    }
    
    if(atoi(receive_buffer) < 10 || atoi(receive_buffer) == 99)
    {
        log_err("NBIOT has not a signal..\n");
        return -1;
    }
    log_nrml("AT+CSQ:%s\n",receive_buffer);
    return 0;

}


int atcmd_cimi(t_comport *comport,char *cimi,int size)
{

    if(!comport)
    {
        
        log_err("%s: invalid_argument\n",__func__);
        return -1;
    }

    char            send_buffer[BUFFER_SIZE];
    char            receive_buffer[BUFFER_SIZE];
    int             rv;

    memset(send_buffer,0,sizeof(send_buffer));
    memset(receive_buffer,0,sizeof(receive_buffer));

    rv = send_atcmd(comport,"at+cimi\r\n",send_buffer,sizeof(send_buffer),TIMEOUT);
    if(rv  < 0)
    {
        log_err("%s: send_atcmd() failed\n",__func__);
        return rv;
    }
    
    rv  = at_fetch(send_buffer,"\r\n","\r\n",receive_buffer,sizeof(receive_buffer));
    if(rv < 0)
    {
        log_err("%s: at_fetch() failed\n",__func__);
        return rv;
    }

    if(size < strlen(receive_buffer))
    {
        log_err("%s: the specified size isn't enough\n",__func__);
        return -1;
    }

    strncpy(cimi,receive_buffer,strlen(receive_buffer));
    log_nrml("AT+CIMI:%s\n",receive_buffer);
    return 0;

}



int atcmd_nband(t_comport *comport)
{

    if(!comport)
    {
        
        log_err("%s: invalid_argument\n",__func__);
        return -1;
    }

    char            send_buffer[BUFFER_SIZE];
    char            receive_buffer[BUFFER_SIZE];
    int             rv;

    memset(send_buffer,0,sizeof(send_buffer));
    memset(receive_buffer,0,sizeof(receive_buffer));

    rv = send_atcmd(comport,"at+nband=?\r\n",send_buffer,sizeof(send_buffer),TIMEOUT);
    if(rv  < 0)
    {
        log_err("%s: send_atcmd() failed\n",__func__);
        return rv;
    }
    
    rv  = at_fetch(send_buffer,":","\r\n",receive_buffer,sizeof(receive_buffer));
    if(rv < 0)
    {
        log_err("%s: at_fetch() failed\n",__func__);
        return rv;
    }
    
    if(strstr(receive_buffer,")") == NULL)
    {
        log_err("NBIOT is not connected..\n");
        return -1;
    }
    log_nrml("+NBAND:%s\n",receive_buffer);
    return 0;

}


int atcmd_cgsn(t_comport *comport,char *imei,int size)
{

    if(!comport)
    {
        
        log_err("%s: invalid_argument\n",__func__);
        return -1;
    }

    char            send_buffer[BUFFER_SIZE];
    char            receive_buffer[BUFFER_SIZE];
    int             rv;

    memset(send_buffer,0,sizeof(send_buffer));
    memset(receive_buffer,0,sizeof(receive_buffer));

    rv = send_atcmd(comport,"at+cgsn=1\r\n",send_buffer,sizeof(send_buffer),TIMEOUT);
    if(rv  < 0)
    {
        log_err("%s: send_atcmd() failed\n",__func__);
        return rv;
    }
    
    rv  = at_fetch(send_buffer,"+","\r\n",receive_buffer,sizeof(receive_buffer));
    if(rv < 0)
    {
        log_err("%s: at_fetch() failed\n",__func__);
        return rv;
    }
   

    if(size < strlen(receive_buffer))
    {
        log_err("%s: the specified size isn't enough\n",__func__);
        return -1;
    }

    strncpy(imei,receive_buffer,strlen(receive_buffer));

    log_nrml("AT+CGSN:%s\n",receive_buffer);
    return 0;

}


int atcmd_cereg(t_comport *comport)
{

    if(!comport)
    {
        
        log_err("%s: invalid_argument\n",__func__);
        return -1;
    }

    char            send_buffer[BUFFER_SIZE];
    char            receive_buffer[BUFFER_SIZE];
    int             rv;

    memset(send_buffer,0,sizeof(send_buffer));
    memset(receive_buffer,0,sizeof(receive_buffer));

    rv = send_atcmd(comport,"at+cereg?\r\n",send_buffer,sizeof(send_buffer),TIMEOUT);
    if(rv  < 0)
    {
        log_err("%s: send_atcmd() failed\n",__func__);
        return rv;
    }
    
    rv  = at_fetch(send_buffer,":","\r\n",receive_buffer,sizeof(receive_buffer));
    if(rv < 0)
    {
        log_err("%s: at_fetch() failed\n",__func__);
        return rv;
    }
    
    if(strncmp(receive_buffer,"0,1",2) != 0)
    {
        log_err("NBIOT isn't registered..\n");
        return -1;
    }
    log_nrml("AT+CEREG:%s\n",receive_buffer);
    return 0;

}


int atcmd_cgatt(t_comport *comport)
{

    if(!comport)
    {
        
        log_err("%s: invalid_argument\n",__func__);
        return -1;
    }

    char            send_buffer[BUFFER_SIZE];
    char            receive_buffer[BUFFER_SIZE];
    int             rv;

    memset(send_buffer,0,sizeof(send_buffer));
    memset(receive_buffer,0,sizeof(receive_buffer));

    rv = send_atcmd(comport,"at+cgatt?\r\n",send_buffer,sizeof(send_buffer),TIMEOUT);
    if(rv  < 0)
    {
        log_err("%s: send_atcmd() failed\n",__func__);
        return rv;
    }
    
    rv  = at_fetch(send_buffer,":","\r\n",receive_buffer,sizeof(receive_buffer));
    if(rv < 0)
    {
        log_err("%s: at_fetch() failed\n",__func__);
        return rv;
    }
    
    if(strncmp(receive_buffer,"1",1) != 0)
    {
        log_err("NBIOT isn't attached to network..\n");
        return -1;
    }
    log_nrml("AT+cgatt:%s\n",receive_buffer);
    return 0;

}


int atcmd_cgmm(t_comport *comport,char *name,int size)
{

    if(!comport)
    {
        
        log_err("%s: invalid_argument\n",__func__);
        return -1;
    }

    char            send_buffer[BUFFER_SIZE];
    char            receive_buffer[BUFFER_SIZE];
    int             rv;

    memset(send_buffer,0,sizeof(send_buffer));
    memset(receive_buffer,0,sizeof(receive_buffer));

    rv = send_atcmd(comport,"at+cgmm\r\n",send_buffer,sizeof(send_buffer),TIMEOUT);
    if(rv  < 0)
    {
        log_err("%s: send_atcmd() failed\n",__func__);
        return rv;
    }
    
    rv  = at_fetch(send_buffer,"\r\n","\r\n",receive_buffer,sizeof(receive_buffer));
    if(rv < 0)
    {
        log_err("%s: at_fetch() failed\n",__func__);
        return rv;
    }

    if(size < strlen(receive_buffer))
    {
        log_err("%s: the specified size isn't enough\n",__func__);
        return -1;
    }

    strncpy(name,receive_buffer,strlen(receive_buffer));
    log_nrml("name:%s\n",receive_buffer);
    return 0;

}


int atcmd_cgmr(t_comport *comport,char *version,int size)
{

    if(!comport)
    {
        
        log_err("%s: invalid_argument\n",__func__);
        return -1;
    }

    char            send_buffer[BUFFER_SIZE];
    char            receive_buffer[BUFFER_SIZE];
    int             rv;

    memset(send_buffer,0,sizeof(send_buffer));
    memset(receive_buffer,0,sizeof(receive_buffer));

    rv = send_atcmd(comport,"at+cgmr\r\n",send_buffer,sizeof(send_buffer),TIMEOUT);
    if(rv  < 0)
    {
        log_err("%s: send_atcmd() failed\n",__func__);
        return rv;
    }
    
    rv  = at_fetch(send_buffer,"\r\n","\r\n",receive_buffer,sizeof(receive_buffer));
    if(rv < 0)
    {
        log_err("%s: at_fetch() failed\n",__func__);
        return rv;
    }

    if(size < strlen(receive_buffer))
    {
        log_err("%s: the specified size isn't enough\n",__func__);
        return -1;
    }

    strncpy(version,receive_buffer,strlen(receive_buffer));
    log_nrml("version:%s\n",receive_buffer);
    return 0;

}




int atcmd_cgpaddr(t_comport *comport)
{

    if(!comport)
    {
        
        log_err("%s: invalid_argument\n",__func__);
        return -1;
    }

    char            send_buffer[BUFFER_SIZE];
    char            receive_buffer[BUFFER_SIZE];
    int             rv;

    memset(send_buffer,0,sizeof(send_buffer));
    memset(receive_buffer,0,sizeof(receive_buffer));

    rv = send_atcmd(comport,"at+cgpaddr\r\n",send_buffer,sizeof(send_buffer),TIMEOUT);
    if(rv  < 0)
    {
        log_err("%s: send_atcmd() failed\n",__func__);
        return rv;
    }
    
    rv  = at_fetch(send_buffer,":","\r\nOK\r\n",receive_buffer,sizeof(receive_buffer));
    if(rv < 0)
    {
        log_err("%s: at_fetch() failed\n",__func__);
        return rv;
    }
#if 0
    if(strncmp(receive_buffer,"OK",2) != 0)
    {
        log_err("NBIOT isn't attached to network..\n");
        return -1;
    }
#endif
    log_nrml("AT+cgpaddr:%s\n",receive_buffer);
    return 0;

}


int atcmd_ncdp(t_comport *comport,char *ip,char *port)
{

    if(!comport)
    {
        
        log_err("%s: invalid_argument\n",__func__);
        return -1;
    }

    char            send_buffer[BUFFER_SIZE];
    char            receive_buffer[BUFFER_SIZE];
    int             rv;

    memset(send_buffer,0,sizeof(send_buffer));
    memset(receive_buffer,0,sizeof(receive_buffer));

    rv = send_atcmd(comport,"at+ncdp=%s,%s\r\n",send_buffer,sizeof(send_buffer),TIMEOUT);
    if(rv  < 0)
    {
        log_err("%s: send_atcmd() failed\n",__func__);
        return rv;
    }
    
    rv  = at_fetch(send_buffer,":","\r\nOK\r\n",receive_buffer,sizeof(receive_buffer));
    if(rv < 0)

        
    {
        log_err("%s: at_fetch() failed\n",__func__);
        return rv;
    }
#if 0
    if(strncmp(receive_buffer,"OK",2) != 0)
    {
        log_err("NBIOT isn't attached to network..\n");
        return -1;
    }
#endif
    log_nrml("AT+cgpaddr:%s\n",receive_buffer);
    return 0;

}


