/*********************************************************************************
 *      Copyright:  chenyujiang <2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  comport.h
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(11/11/21)
 *         Author:  chenyujiang <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "11/11/21 06:34:45"
 *                 
 ********************************************************************************/

#ifndef __COMPORT_H__
#define __COMPORT_H__


#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <string.h>
#include  <getopt.h>
#include  <fcntl.h>
#include  <errno.h>
#include  <termios.h>
#include  <sys/stat.h>
#include  <sys/wait.h>
#include  <sys/types.h>
#include  <sys/stat.h>
#include  <sys/select.h>

#ifndef DEVICE_NAME_LEN
#define DEVICE_NAME_LEN 128
#endif


typedef struct _t_comport
{
    char                device_name[DEVICE_NAME_LEN];

    int                 fd;

    long                 baudrate;
    char                databits;
    char                parity;
    char                stopbits;
    char                flowcntl;
    int                 frag_size;

    struct termios      original_termios;

    unsigned char       is_used;

}t_comport;

extern t_comport *comport_init(const char *device_name,long baudrate,char *settings);

extern int comport_open(t_comport *comport);

extern int comport_close(t_comport *comport);

extern int comport_write(t_comport *comport,char *buf,int buf_size);

extern int comport_read(t_comport *comport,char *buf,int buf_size,int timeout);

extern void set_baudrate(struct termios *term,long baudrate);

extern void set_databits(struct termios *term,char databits);

extern void set_parity(struct termios *term,char parity);

extern void set_stopbits(struct termios *term,char stopbits);

extern void set_flowcntl(struct termios *term,char flowcntl);


#endif
