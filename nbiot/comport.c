/*********************************************************************************
 *      Copyright:  chenyujiang <2631336290@qq. com>
 *                  All rights reserved.
 *
 *       Filename:  comport.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(11/11/21)
 *         Author:  chenyujiang <2631336290@qq.    com>
 *      ChangeLog:  1, Release initial version on "11/11/21 07:00:03"
 *                 
 ********************************************************************************/

#include "comport.h"

t_comport *comport_init(const char *          device_name,long baudrate,char *settings)
{

    if(!device_name || !baudrate || !settings)
    {
        printf("%s: invalid_argument\n",__func__);
        return NULL;
    }

    t_comport *comport = NULL;

    if(NULL == (comport = (t_comport *)malloc(sizeof(t_comport))))
    {
        printf("%s: memory malloc failed: %s\n",__func__,strerror(errno));
        return NULL;
    }

    memset(comport,0,sizeof(*comport));
    
    comport->is_used    = 0x00;
    comport->fd         = -1;
    comport->baudrate   = baudrate;
    comport->databits   = settings[0];
    comport->parity     = settings[1];
    comport->stopbits   = settings[2];
    comport->flowcntl   = settings[3];
    comport->frag_size  = 128;
    strncpy(comport->device_name,device_name,strlen(device_name));
    //printf("TTY argument [baudrate: %ld,databits: %c,parity: %c,stopbits: %c,flowcntl: %c]\n",comport->baudrate,comport->databits,comport->parity,comport->stopbits,comport->flowcntl);

    return comport;
}

int comport_open(t_comport *comport)
{
    if(NULL == comport)
    {
        printf("%s: invalid_argument\n",__func__);
        return -1;
    }
    int rv = -1;
    struct termios new_termios;

    comport->fd = open(comport->device_name,O_RDWR | O_NOCTTY | O_NONBLOCK);

    if(comport->fd < 0)
    {
        printf("%s: open tty failed:%s\n",__func__,strerror(errno));
        return -1;
    }

    if(fcntl(comport->fd,F_SETFL,0) < 0)
    {
        printf("%s: fcntl check failed\n",__func__);
        return -1;
    }

    if(0 == isatty(comport->fd))
    {
        printf("%s: %s[%d] is not a terminal        equipment\n",__func__,comport->device_name,comport->fd);
        return -1;
    }

    if(tcgetattr(comport->fd,&(comport->original_termios)))
    {
        printf("%s: get original termios             attribution false: %s\n",__func__,                   strerror(errno));
        return -1;
    }

    if(tcgetattr(comport->fd,&new_termios))
    {
        printf("%s: tcgetattr() failed: %s\n",__func__,strerror(errno));
        return -1;
    }

    new_termios.c_cflag |= CLOCAL;
    new_termios.c_cflag |= CREAD;
    new_termios.c_cflag &= ~CSIZE;
    new_termios.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    new_termios.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    new_termios.c_oflag &= ~(OPOST);

    set_databits(&new_termios,comport->databits);
    set_parity(&new_termios,comport->parity);
    set_stopbits(&new_termios,comport->stopbits);
    set_flowcntl(&new_termios,comport->flowcntl);

    new_termios.c_cc[VMIN] = 0;
    new_termios.c_cc[VTIME] = 0;

    tcflush(comport->fd,TCIOFLUSH);

    if(tcsetattr(comport->fd,TCSANOW,&new_termios))
    {
        printf("%s, tcsetattr failed:%s\n",          __func__, strerror(errno));
        return -1;
    }

    comport->is_used = 0x01;
    
    return comport->fd;

}


int comport_close(t_comport *comport)
{
    if(NULL == comport)
    {
        printf("%s: invalid_argument\n",__func__);
        return -1;
    }

    if(tcsetattr(comport->fd,TCSANOW,&(comport->original_termios)))
    {
        printf("%s: tcsetattr() failed: %S\n",__func__,strerror(errno));
        return -1;
    }

    if(comport->fd >= 0)
    {
        close(comport->fd);

        comport->is_used = 0x00;

        comport->fd = -1;
    }

    memset(comport,0,sizeof(*comport));
    free(comport);
    comport = NULL;
    return 0;
}

int comport_write(t_comport *                   comport,         char *buf,int buf_size)
{
    char *start,*end;
    int rv = 0;
    int send = 0;

    if(!buf || buf_size <= 0)
    {
        printf("%s: invalid_argument\n",__func__);
        rv = -1;
        goto cleanup;
    }

    if(0x01 != comport->is_used)
    {
        printf("device not connected\n");
        rv = -2;
        goto cleanup;
    }

    if(comport->frag_size < buf_size)
    {
        start = buf;
        end = buf + buf_size;

        do
        {
            if(comport->frag_size < (end - start))
            {
                send = write(comport->fd,start,comport->frag_size);
                if(send <= 0)
                {
                    rv = -4;
                    goto cleanup;
                }
                start += comport->frag_size;

            }
            else
            {
                send = write(comport->fd,start,(end - start));
                if(send <= 0)
                {
                    rv = -4;
                    goto cleanup;
                }
                start += (end - start);

            }
        }while(start < end);
    }
    else
    {
        send = write(comport->fd,buf,buf_size);
        if(send <= 0)
        {
            rv = -5;
            goto cleanup;
        }
    }

cleanup:
    return rv;

}

int comport_read(t_comport *comport,            char *    buf,int buf_size,int timeout)
{
    fd_set              rfds;
    struct timeval      time_out;
    struct timeval      *time;
    int                 rv;

    char                *ptr = buf;
    
    
    if(!comport || !buf || buf_size <= 0)
    {
        printf("%s: invalid_argument\n",__func__);
        return -1;
    }

    if(comport->is_used != 0x01)
    {
        printf("device not connected\n");
        return -2;
    }
        
    time_out.tv_sec = (time_t)(timeout/1000);
    time_out.tv_usec = (long)(1000*(timeout%1000));
    time = timeout > 0 ? &time_out : NULL;

    //FD_ZERO(&rfds);
    //FD_SET(comport->fd,&rfds);
    
    while(1)
    {
        FD_ZERO(&rfds);
        FD_SET(comport->fd,&rfds);
        rv = select(comport->fd+1,&rfds,NULL,NULL,time);
     
        if(rv < 0)
        {
            printf("%s: select() failed: %s\n",__func__,strerror(errno));
            break;

        }

        else if(rv == 0)
        {
            printf("%s: timeout\n",__func__);
            break;
        }
        else if(rv > 0)
        {
            rv = read(comport->fd,ptr,buf_size);
            if(rv <= 0)
            {
                printf("%s: read() failed: %s\n",__func__,strerror(errno));
                return -1;
            }
            ptr += rv;
            buf_size -= rv;
            if(strstr(buf,"\nOK\r\n"))
            {
                break;
            }

            if(strstr(buf,"\nERROR\r\n"))
            {
                break;
            }
        }
    }


    return rv;
}

void set_baudrate(struct termios *term,       long baudrate)
{
    switch(baudrate)
    {
        case 2400:
            cfsetispeed(term,B2400);
            cfsetospeed(term,B2400);
            break;
    
        case 4800:
            cfsetispeed(term,B4800);
            cfsetospeed(term,B4800);
            break;
    
        case 9600:
            cfsetispeed(term,B9600);
            cfsetospeed(term,B9600);
            break;
    
        case 38400:
            cfsetispeed(term,B38400);
            cfsetospeed(term,B38400);
            break;

        case 57600:
            cfsetispeed(term,B57600);
            cfsetospeed(term,B57600);
            break;

        case 19200:
            cfsetispeed(term,B19200);
            cfsetospeed(term,B19200);
            break;

        case 115200:
            cfsetispeed(term,B115200);
            cfsetospeed(term,B115200);
            break;

        default:
            cfsetispeed(term,B9600);
            cfsetospeed(term,B9600);
            break;
    }
}


void set_databits(struct termios *term,       char databits)
{
    switch(databits)
    {
        case 8:
            term->c_cflag |= CS8;
            break;
   
        case 7:
            term->c_cflag |= CS7;
            break;
        case 6:
            term->c_cflag |= CS6;
            break;
        case 5:
            term->c_cflag |= CS5;
            break;
        default:
            term->c_cflag |= CS8;
            break;
    }
}

void set_parity(struct termios *term,char     parity)
{
    switch(parity)
    {
        case 'N':
        case 'n':
            term->c_cflag &= ~PARENB;
            break;

        case 'E':
        case 'e':
            term->c_cflag |= PARENB;
            term->c_cflag &= ~PARODD;
            term->c_iflag |= (INPCK | ISTRIP);
            break;

        case 'O':
        case 'o':
            term->c_cflag |= (PARENB | PARODD);
            term->c_iflag |= (INPCK | ISTRIP);
            break;

        default:
            term->c_cflag &= ~PARENB;
            break;
    }
}


void set_stopbits(struct termios *term,       char stopbits)
{
    switch(stopbits)
    {
        case 2:
            term->c_cflag |= CSTOPB;
            break;

        default:
            term->c_cflag &= ~CSTOPB;
            break;
    }
}

void set_flowcntl(struct termios *term,       char flowcntl)
{
    switch(flowcntl)
    {
        case 'S':
        case 's':
        case 'B':
        case 'b':
            term->c_cflag &= ~(CRTSCTS);
            term->c_iflag |= (IXON | IXOFF);
            break;

        case 'H':
        case 'h':
            term->c_cflag |= CRTSCTS;
            term->c_iflag &= ~(IXON | IXOFF);
            break;

        default:
            term->c_cflag &= ~(CRTSCTS);
            term->c_iflag |= (IXON | IXOFF);
            break;
    }
}


