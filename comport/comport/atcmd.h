/********************************************************************************
 *      Copyright:  (C) 2021 chenyujiang <2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  atcmd.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(11/12/21)
 *         Author:  chenyujiang <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "11/12/21 07:53:22"
 *                 
 ********************************************************************************/

#ifndef __ATCMD_H__
#define __ATCMD_H__

#include "comport.h"

extern int send_atcmd(t_comport *comport,char *atcmd,char *buffer,int buffer_size,int timeout);


extern int at_fetch(char *at_reply,char *start_key,char *end_key,char  *buffer,int buffer_size);



#endif
