/*********************************************************************************
 *      Copyright:  (C) 2021 chenyujiang <2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  microcom.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(10/31/21)
 *         Author:  chenyujiang <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "11/11/21 04:22:15"
 *                 
 ********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <getopt.h>
#include <libgen.h>
#include <signal.h>
#include "comport.h"
#include <time.h>

#include "comport.h"


int     g_stop = 0;


static inline void usage(char *progname);


void sighandler(int signum)
{
    g_stop = 1;
}


int main (int argc, char **argv)
{
    char                    *progname = basename(argv[0]);
    int                     opt = 0;
    int                     rv = -1;
    int                     i;

    t_comport               *comport;
    long                    baudrate = 9600;
    char                    *settings = "8N1N";
    char                    *device_name = NULL;
    char                    send_buffer[512];
    char                    receive_buffer[512];

    int                     fd_com = -1;

    fd_set                  rfds;

    struct  option  options[] = 
    {
        {"device",required_argument,NULL,'d'},
        {"baudrate",required_argument,NULL,'b'},
        {"settings",required_argument,NULL,'s'},
        {"help",no_argument,NULL,'h'},
        {NULL,0,NULL,0}
    };

    
    while((opt = getopt_long(argc,argv,"d:b:s:h",options,NULL)) != -1)
    {
        switch(opt)        
        {
            case 'd':
                device_name = optarg;
                break;

            case 'b':
                baudrate = atoi(optarg);
                break;

            case 's':
                settings = optarg;
                break;

            case 'h':
                usage(progname);
                return 0;
            
            default:
                break;

        }
    }

    if(!device_name)
    {
        usage(basename(argv[0]));
        return 0;
    }

    if((comport = comport_init(device_name,baudrate,settings)) == NULL)
    {
        return -1;
    }

    if((fd_com = comport_open(comport))< 0)
    {
        return -1;
    }

    signal(SIGINT,sighandler);

    fflush(stdin);

    while(!g_stop)
    {
        FD_ZERO(&rfds);
        FD_SET(STDIN_FILENO,&rfds);
        FD_SET(fd_com,&rfds);
        rv = select(fd_com + 1,&rfds,NULL,NULL,NULL);
        if(rv < 0)
        {
            break;
        }
         
        if(FD_ISSET(STDIN_FILENO,&rfds))
        {
            memset(send_buffer,0,sizeof(send_buffer));
            fgets(send_buffer,sizeof(send_buffer),stdin);
            //send_buffer[strlen(send_buffer)-1] = '\r';
            //int i = strlen(send_buffer);
            if(comport_write(comport,send_buffer,strlen(send_buffer)) < 0)
            {
                break;
            }
            //printf("send data : %s\n",send_buffer);
            fflush(stdin);
        }
        if(FD_ISSET(comport->fd,&rfds))
        {
            //printf("start to receive data: \n");
            memset(receive_buffer,0,sizeof(receive_buffer));
            rv = comport_read(comport,receive_buffer,sizeof(receive_buffer),50);
            
            if(rv <  0)
            {
                 goto cleanup;
            }
            printf("%s",receive_buffer);
        }
    }


cleanup:
    comport_close(comport);
    printf("bye!\n");
    return 0;
} 


void usage(char *progname)
{
    printf("%s usage: comport -d <device> [-b <baudrate>] [-s <settings>] [-x]\n",progname);
    printf("-d <device>     device name\n");
    printf("-b <baudrate>   device baudrate (115200,57600,19200,9600),default is 115200\n");
    printf("-s <settings>   device settings ,for instance: 8N1N(default settings)\n");
    printf("                                                -databits: 8,7\n");
    printf("                                                -parity: N=None,O=Odd,E=Even,S=Space\n");
    printf("                                                -stopbits:1,0\n");
    printf("                                                -flow control: N=None,H=Hardware,S=Software,B=Both\n");
    printf("-h <help>        display this usage\n");
}

