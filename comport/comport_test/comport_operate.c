/*********************************************************************************
 *      Copyright:  (C) 2021 LingYun IoT System Studio
 *                  All rights reserved.
 *
 *       Filename:  comport_operate.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(10/31/21)
 *         Author:  RaspberryPi <lingyun@email.com>
 *      ChangeLog:  1, Release initial version on "10/31/21 07:54:35"
 *                 
 ********************************************************************************/
#include <sys/select.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>

#include "comport_operate.h"


int comport_open(comport_attr *attr,    char *serialname,long baud_rate,char *          settings)
{
    if(!attr || !serialname || !baud_rate || !settings)
    {
        printf("%s :invalid_argument\n",__func__);
        return -1;
    }


    memset(attr,0,sizeof(*attr));
    attr->fd    =   -1;
    attr->len_per_send  =   128;
    strncpy(attr->serialname,serialname,strlen(serialname));
    attr->baud_rate = baud_rate;
    attr->databits  =   settings[0];
    attr->parity    =   settings[1];
    attr->stopbits  =   settings[2];
    attr->flowcntl  =   settings[3];

    attr->fd = open(serialname,O_RDWR | O_NOCTTY | O_NONBLOCK);

    if(attr->fd < 0)
    {
        printf("%s: get comport file description failed: %s\n",__func__,strerror(errno));
        return -2;
    }

    if(fcntl(attr->fd,F_SETFL,0) < 0)
    {
        printf("%s :check %s[%d] is block or not failed: %s\n",__func__,attr->serialname,attr->fd,strerror(errno));
        return -3;
    }

    if(0 == isatty(attr->fd))
    {
        printf("%s: %s[%d] is not a terminal equipment\n",__func__,attr->serialname,attr->fd);
        return -4;
    }

    if(tcgetattr(attr->fd,&attr->comport_termios))
    {
        printf("%s: get original termios attribution false: %s\n",__func__,strerror(errno));
        return -5;
    }
#if 0
    if(tcgetattr(attr->fd,&attr->original_termios))
    {
        printf("%s: get original termios attribution false: %s\n",__func__,strerror(errno));
        return -5;
    }
#endif
    attr->comport_termios.c_cflag |= CLOCAL;
    attr->comport_termios.c_cflag |= CREAD;
    attr->comport_termios.c_cflag &= ~CSIZE;
    attr->comport_termios.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    attr->comport_termios.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    attr->comport_termios.c_oflag &= ~OPOST;


    switch (attr->baud_rate)
    {
        case 115200:
            cfsetispeed(&attr->comport_termios,B115200);
            cfsetospeed(&attr->comport_termios,B115200);
            break;

         case 57600:
            cfsetispeed(&attr->comport_termios,B57600);
            cfsetospeed(&attr->comport_termios,B57600);
            break;

        case 38400:
            cfsetispeed(&attr->comport_termios,B38400);
            cfsetospeed(&attr->comport_termios,B38400);
            break;

        case 19200:
            cfsetispeed(&attr->comport_termios,B19200);
            cfsetospeed(&attr->comport_termios,B19200);
            break;

        case 9600:
            cfsetispeed(&attr->comport_termios,B9600);
            cfsetospeed(&attr->comport_termios,B9600);
            break;

        case 4800:
            cfsetispeed(&attr->comport_termios,B4800);
            cfsetospeed(&attr->comport_termios,B4800);
            break;

        case 2400:
            cfsetispeed(&attr->comport_termios,B2400);
            cfsetospeed(&attr->comport_termios,B2400);
            break;

    }

    switch (attr->databits)
    {
        case '5':
            attr->comport_termios.c_cflag |= CS5;
            break;

        case '6':
            attr->comport_termios.c_cflag |= CS6;
            break;

        case '7':
            attr->comport_termios.c_cflag |= CS7;
            break;

        case '8':
            attr->comport_termios.c_cflag |= CS8;
            break;
            
        default:
            attr->comport_termios.c_cflag |= CS8;
    }

    switch(attr->parity)
    {
        case 'n':
        case 'N':
            attr->comport_termios.c_cflag &=    ~PARENB;
            //attr->comport_termios.c_iflag &=    ~INPCK;
            break;
        
        case 'e':
        case 'E':
            attr->comport_termios.c_cflag |=    PARENB;
            attr->comport_termios.c_cflag &=    ~PARODD;
            attr->comport_termios.c_iflag |=    INPCK | ISTRIP;
            break;

        case 'o':
        case 'O':
            attr->comport_termios.c_cflag |=    PARENB;
            attr->comport_termios.c_cflag |=    PARODD;
            attr->comport_termios.c_iflag |=    INPCK | ISTRIP;

        case 's':
        case 'S':
            attr->comport_termios.c_cflag &=    ~PARENB;
            attr->comport_termios.c_cflag &=    CSTOPB;
           break;

        default:
            attr->comport_termios.c_cflag &=    ~PARENB;
            //attr->comport_termios.c_iflag &=    ~INPCK;
            break;
    }

    switch(attr->stopbits)
    {
        case '1':
            attr->comport_termios.c_cflag &=    ~CSTOPB;
            break;

        case '2':
            attr->comport_termios.c_cflag |=    CSTOPB;
            break;
 
        default:
            attr->comport_termios.c_cflag &=    ~CSTOPB;
            break;   
    }

    switch(attr->flowcntl)
    {
        case 's':
        case 'S':
            attr->comport_termios.c_cflag &= ~(CRTSCTS);
            attr->comport_termios.c_iflag |= (IXON | IXOFF);
            break;

        case 'H':
        case 'h':
            attr->comport_termios.c_cflag |= CRTSCTS;
            attr->comport_termios.c_iflag &= ~(IXON | IXOFF);
            break;

        case 'B':
        case 'b':
            attr->comport_termios.c_cflag &= ~(CRTSCTS);
            attr->comport_termios.c_iflag |= (IXON | IXOFF);
            break;

        case 'N':
        case 'n':
            attr->comport_termios.c_cflag &= ~(CRTSCTS);
            //attr->comport_termios.c_iflag &= ~(IXON | IXOFF);
            attr->comport_termios.c_iflag |= (IXON | IXOFF);
            break;

        default:
            attr->comport_termios.c_cflag &= ~(CRTSCTS);
            //attr->comport_termios.c_cflag &= ~(IXON |IXOFF);
            attr->comport_termios.c_iflag |= (IXON | IXOFF);
            break;
    }

    attr->comport_termios.c_cc[VTIME] = 0;
    attr->comport_termios.c_cc[VMIN] = 0;

    if(tcflush(attr->fd,TCIFLUSH))
    {
        printf("%s failed: %s\n","tcflush",strerror(errno));
        return -5;
    }

    if(tcsetattr(attr->fd,TCSANOW,&attr->comport_termios) != 0)
    {
        printf("%s, tcsetattr failed:%s\n", __func__, strerror(errno));
        return -5;
    }
    
//    printf("comport init successfully .........\n");
    return 0;

}

int comport_close(comport_attr *        attr)
{
    if(!attr)
    {
        printf("%s : invalid_argument\n",__func__);
        return -1;
    }
#if 0
    if(tcflush(attr->fd,TCIOFLUSH));
    {
        printf("%s, tcsetattr failed:%s\n", __func__, strerror(errno));
        return -5;
    }
#endif

    if(attr->fd >= 0)
    {

#if 0
        if(tcsetattr(attr->fd,TCSANOW,&attr->original_termios))
        {
            printf("%s: restore original_termios attribution failed: %s\n",__func__,strerror(errno));
            return -5;
        }
#endif
        close(attr->fd);
    }
    attr->fd = -1;
    return 0;
}

int comport_send(comport_attr *attr,    char *msg,int size)
{
    if(!attr || !msg || size <= 0)
    {
        printf("%s : invalid_argument\n",__func__);
        return -1;
    }

    char        *ptr;
    int         left_bytes = 0;
    int         rv = -1;
    int         wrlen = 0;

    ptr = msg;
    left_bytes = size;

    while(left_bytes > 0)
    {
        wrlen = left_bytes > attr->len_per_send ? attr->len_per_send : left_bytes;
        rv = write(attr->fd,ptr,wrlen);
        if(rv <= 0)
        {
            printf("%s: write to comport[attr->fd] failed: %s\n",__func__,strerror(errno));
            return -2;
        }
        ptr += rv;
        left_bytes -= rv;
    }

    return rv;
}


int comport_recv(comport_attr *attr,    char *buffer,int size,int timeout)
{
    int                 rv;
    fd_set              rfds;
    struct  timeval     time_out;
    struct  timeval     *time;
    char *ptr = buffer;


    if(!attr || !buffer || size <= 0)
    {
        printf("%s: invalid_argument\n",__func__);
        return -1;
    }

    //printf("fd=%d timeout: %d\n",attr->fd,timeout);


    time_out.tv_sec = 0;
    time_out.tv_usec = timeout*1000;
    time = timeout > 0 ? &time_out : NULL;
    FD_ZERO(&rfds);
    FD_SET(attr->fd,&rfds);


    while(1)
    {
        
       // printf ("before select\n");
        rv = select(attr->fd + 1,&rfds,NULL,NULL,time);
        
        if(rv < 0)
        {
            printf("%s: select failed: %s\n",__func__,strerror(errno));
            break;
            
        }

        else if(rv > 0)
        {
           // usleep(10000);
            rv = read(attr->fd,ptr,size);
            if(rv <= 0)
            {
                printf("%s: read failed: %s\n",__func__,strerror(errno));
                break;
            }
            ptr += rv;
            size -= rv;
            if(strstr ( buffer, "OK\r\n"))
            {
                break;
            }
            if(strstr( buffer, "ERROR\r\n") )
            {
                break;
            }
        }

        else if(rv == 0)
        {
            printf("timeout!\n");
            break;
        }
    }
    
        //printf ("after select\n");
    return rv;

}



