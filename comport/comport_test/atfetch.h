/********************************************************************************
 *      Copyright:  (C) 2021 LingYun IoT System Studio
 *                  All rights reserved.
 *
 *       Filename:  atfetch.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(11/06/21)
 *         Author:  RaspberryPi <lingyun@email.com>
 *      ChangeLog:  1, Release initial version on "11/06/21 09:16:41"
 *                 
 ********************************************************************************/

#ifndef __ATFETCH_H__
#define __ATFETCH_H__

extern int at_fetch(char *at_reply,char *start_key,char *end_key,char *rbuffer,int size);


#endif
