/*********************************************************************************
 *      Copyright:  (C) 2021 LingYun IoT System Studio
 *                  All rights reserved.
 *
 *       Filename:  atfetch.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(11/06/21)
 *         Author:  RaspberryPi <lingyun@email.com>
 *      ChangeLog:  1, Release initial version on "11/06/21 09:19:10"
 *                 
 ********************************************************************************/


#include <stdio.h>
#include <string.h>





#include "atfetch.h"

int at_fetch(char *atreply, char *start_key,char *end_key,char *rbuffer,int size)
{
    char            *rbuffer_start;
    char            *rbuffer_end;
    int             rv = -1;

    if(rbuffer != NULL)
    {
        memset(rbuffer,0,sizeof(*rbuffer));

    }

    rbuffer_start = strstr(atreply,start_key);

    if(!rbuffer_start)
    {
        printf("strstr start_key: %s failed\n",start_key);
        return -1;
    }

    rbuffer_start += strlen(start_key);

    if(!end_key)
    {
        rbuffer_end = strstr(rbuffer_start,end_key);
        if(!rbuffer_end)
        {

            printf ("strstr end_key: %s failed\n",end_key);
            return -2;
        }
    }

    if(rbuffer  != NULL && (rbuffer_end-rbuffer_start) <= size)
    {
        strncpy(rbuffer,rbuffer_start,rbuffer_end-rbuffer_start);
    }

    return 0;
}
