/********************************************************************************
 *      Copyright:  (C) 2021 LingYun IoT System Studio
 *                  All rights reserved.
 *
 *       Filename:  comport_operate.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(10/31/21)
 *         Author:  RaspberryPi <lingyun@email.com>
 *      ChangeLog:  1, Release initial version on "10/31/21 04:56:27"
 *                 
 ********************************************************************************/

#ifndef _COMPORTOPERATE_H_
#define _COMPORTOPERATE_H_

#define SERIAL_NAME_SIZE 128

typedef struct comport_attr
{
    int                 fd;             //comport file description
    long                 baud_rate;
    char                databits;
    char                parity;
    char                stopbits;
    int                 len_per_send;   //maximum length percent send
    char                serialname[SERIAL_NAME_SIZE];
    char                flowcntl;
    struct  termios     comport_termios;
    struct  termios     original_termios;
}comport_attr;


extern int comport_open(comport_attr *attr,char *serialname,long baud_rate,char *settings);
extern int comport_close(comport_attr *attr);
extern int comport_send(comport_attr *attr,char *msg,int size);
extern int comport_recv(comport_attr *attr,char *buffer,int size,int timeout);



#endif
