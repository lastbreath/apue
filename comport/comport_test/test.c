/*********************************************************************************
 *      Copyright:  (C) 2021 LingYun IoT System Studio
 *                  All rights reserved.
 *
 *       Filename:  microcom.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(10/31/21)
 *         Author:  RaspberryPi <lingyun@email.com>
 *      ChangeLog:  1, Release initial version on "10/31/21 04:22:15"
 *                 
 ********************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include<stdlib.h>
#include <termios.h>
#include <string.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <libgen.h>
#include <sys/ioctl.h>
#include <signal.h>


#include "comport_operate.h"


int     g_stop = 0;


static inline void usage(char *progname);
void sighandler(int signum)
{
    g_stop = 1;
}





int main (int argc, char **argv)
{
    char                    *progname = basename(argv[0]);
    int                     opt = 0;
    int                     rv = -1;
    int                     i;

    comport_attr            attr;
    long                    baudrate = 9600;
    char                    *settings = "8N1N";
    char                    *device_name = NULL;
    char                    send_buffer[512];
    char                    receive_buffer[512];

    fd_set                  rfds;

    struct  option  options[] = 
    {
        {"device",required_argument,NULL,'d'},
        {"baudrate",required_argument,NULL,'b'},
        {"settings",required_argument,NULL,'s'},
        {"help",no_argument,NULL,'h'},
        {NULL,0,NULL,0}
    };

    
    while((opt = getopt_long(argc,argv,"d:b:s:h",options,NULL)) != -1)
    {
        switch(opt)        
        {
            case 'd':
                device_name = optarg;
                break;

            case 'b':
                baudrate = atoi(optarg);
                break;

            case 's':
                settings = optarg;
                break;

            case 'h':
                usage(progname);
                return 0;
            
            default:
                break;

        }
    }

    if(!device_name)
    {
        usage(basename(argv[0]));
        return 0;
    }

    if(comport_open(&attr,device_name,baudrate,settings) < 0)
    {
        return -1;
    }

    signal(SIGINT,sighandler);
    fflush(stdin);
    while(!g_stop)
    {
        FD_ZERO(&rfds);
        FD_SET(STDIN_FILENO,&rfds);
        FD_SET(attr.fd,&rfds);
        rv = select(attr.fd + 1,&rfds,NULL,NULL,NULL);
        if(rv < 0)
        {
            break;
        }
            
        if(FD_ISSET(STDIN_FILENO,&rfds))
        {
            memset(send_buffer,0,sizeof(send_buffer));
            fgets(send_buffer,sizeof(send_buffer),stdin);
            //send_buffer[strlen(send_buffer)-1] = '\r';
            //int i = strlen(send_buffer);
            //printf("%d\n",i);
            //strcpy(&send_buffer[i-1],"\r");
            if(comport_send(&attr,send_buffer,strlen(send_buffer)) < 0)
            {
                goto cleanup;
            }
            //printf("send data : %s\n",send_buffer);
            fflush(stdin);
        }
        if(FD_ISSET(attr.fd,&rfds))
        {
            //printf("start to receive data: \n");
            memset(receive_buffer,0,sizeof(receive_buffer));
            rv = comport_recv(&attr,receive_buffer,sizeof(receive_buffer),50);
            
            if(rv <  0)
            {
                goto cleanup;
            }
            printf("%s",receive_buffer);
           // fflush(stdout);
        }
    }


cleanup:
    comport_close(&attr);
    printf("bye!\n");
    return 0;
} 


void usage(char *progname)
{
    printf("%s usage: comport -d <device> [-b <baudrate>] [-s <settings>] [-x]\n",progname);
    printf("-d <device>     device name\n");
    printf("-b <baudrate>   device baudrate (115200,57600,19200,9600),default is 115200\n");
    printf("-s <settings>   device settings ,for instance: 8N1N(default settings)\n");
    printf("                                                -databits: 8,7\n");
    printf("                                                -parity: N=None,O=Odd,E=Even,S=Space\n");
    printf("                                                -stopbits:1,0\n");
    printf("                                                -flow control: N=None,H=Hardware,S=Software,B=Both\n");
    printf("-h <help>        display this usage\n");
}

