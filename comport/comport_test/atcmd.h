/********************************************************************************
 *      Copyright:  (C) 2021 LingYun IoT System Studio
 *                  All rights reserved.
 *
 *       Filename:  atcmd.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(11/06/21)
 *         Author:  RaspberryPi <lingyun@email.com>
 *      ChangeLog:  1, Release initial version on "11/06/21 08:37:56"
 *                 
 ********************************************************************************/

#ifndef __ATCMD_H__
#define __ATCMD_H__


extern int sr_atcmd(comport_attr *attr,char *atcmd,char *buffer,int buffer_size,int timeout);


#endif
