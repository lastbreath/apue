/*********************************************************************************
 *      Copyright:  (C) 2021 LingYun IoT System Studio
 *                  All rights reserved.
 *
 *       Filename:  atcmd.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(11/06/21)
 *         Author:  RaspberryPi <lingyun@email.com>
 *      ChangeLog:  1, Release initial version on "11/06/21 08:58:14"
 *                 
 ********************************************************************************/




#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <stirng.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>



#include "comport_operate.h"



int sr_atcmd(comport_attr *attr,char *atcmd,char *buffer,int buffer_size,int timeout)
{
    char                *ptr;
    int                 rv = -1;
    int                 bytes = 0;

    fd_set              rfds;
    struct timeval      timeout;

    if(!attr || !atcmd || !buffer || buffer <= 0)
    {
        printf("%s: invalid_argument\n",__func__);
        return -1;

    }

    comport_send(attr,atcmd,strlen(atcmd));

    memset(buffer,0,buffer_size);

    comport_recv(attr,buffer,sizeof(buffer),300);
}
