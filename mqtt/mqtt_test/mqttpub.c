/*********************************************************************************
 *      Copyright:  (C) 2021 lastbreath<2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  socketserv_epoll.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(08/08/2021)
 *         Author:
 *
 *         lastbreath <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "08/08/2021 05:08:41 PM"
 *                 
 ********************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <netdb.h>
#include "mosquitto.h"
#include <libgen.h>
#include "ds18b20.h"
#include <time.h>
#include <signal.h>

#define BUF_SZIE  1024
#define ARRY_SIZE(x)    (sizeof(x)/sizeof(x[0]))
#define KEEP_ALIVE 60


int g_stop = 0;

static inline void print_usage(char *progname);
void sig_handler(int sig_num)
{
    if(sig_num == SIGUSR1)
        g_stop = 1;

}




int main (int argc, char **argv)
{

    char *progname = basename(argv[0]);
    int port;
    char *ip = NULL;
    char *hostname = NULL;
    char *topic = NULL;
    struct hostent *hostentp;
    struct mosquitto *mosq = NULL;
    int daemon_run = 0;
    int opt;
    int rv;
    float temperature = 0;
    char buffer[BUF_SZIE];
    time_t  t;
    struct tm *timep;
    int log_fd;
    int connect_flag = 1;
     struct option options[] = 
     {
         {"daemon",no_argument,NULL,'d'},
         {"topic", required_argument,NULL,'t'},
         {"hostname", required_argument,NULL,'n'},
         {"ip", required_argument, NULL, 'i'},
         {"port",required_argument,NULL,'p'},
         {"help",no_argument,NULL,'h'},
         {NULL,0,NULL,0}
     };



     while((opt = getopt_long(argc,argv,"dhp:t:i:n:",options,NULL)) != -1)
     {
        
         switch(opt)
         
         {

             case 't':
                 topic = optarg;
                 break;

             case 'i':
                 ip = optarg;
                 break;

             case 'n':
                 hostname = optarg;
                 break;


             case 'd':
                 daemon_run = 1;
                 break;
             case 'p':
                 port = atoi(optarg);
                 break;
             case 'h':
                 print_usage(argv[0]);
                 return 0;
             default:
                 break;
         }
     }

     if(!port)
     {
         print_usage(progname);
         return -1;
     }

     if(!hostname && !ip)
     {
        print_usage(progname);
        return -1;
     }
    
    
     if(hostname)
     {
         hostentp = gethostbyname(hostname);
         if(!hostentp)
         {
             printf("Failed to get host by name: %s\n",strerror(errno));
             return -2;
         }
        printf("hostname: %s\n",hostentp->h_name);
        ip = inet_ntoa(*(struct in_addr *)hostentp->h_addr);
        printf("address: %s\n",ip);

     }

    if(!topic)
    {
        topic = "temperature";
    }

     if(daemon_run)
     {
         printf("program %s running in backgrund\n", progname);
         if( (log_fd = open("client.log", O_CREAT|O_RDWR, 0666)) < 0)
         {
             printf("open() failed:%s\n", strerror(errno)) ;
             return -2;
         }

        dup2(log_fd, STDOUT_FILENO) ;
        dup2(log_fd, STDERR_FILENO) ;

         daemon(1,1);
     }
     
    
     signal(SIGUSR1,sig_handler);
     
     mosquitto_lib_init();
     mosq = mosquitto_new(NULL,true,NULL);

     if(!mosq)
     {
         printf("mosquitto_new() failed: %s\n",strerror(errno));
         mosquitto_lib_cleanup();
         return -1;
     }
     printf("Create mosquitto successfully!\n");
     if(mosquitto_connect(mosq,ip,port,KEEP_ALIVE) != MOSQ_ERR_SUCCESS)
     {

          printf("mosquitto_connect() failed: %s\n",strerror(errno));
          mosquitto_destroy(mosq);
          mosquitto_lib_cleanup();
          return -1;
     }

     printf("connect %s:%d successfully!\n",ip,port);
     if(mosquitto_loop_start(mosq) != MOSQ_ERR_SUCCESS)
     {

                printf("mosquitto_loop_start() failed: %s\n",strerror(errno));
                mosquitto_destroy(mosq);
                mosquitto_lib_cleanup();
     }

     while(!g_stop)
     {
        
         if(ds18b20_get_temperature(&temperature) < 0)
         {
             printf("ds18b20_get_temperature failed.\n");
             return -3;
         }

        time(&t);
        //timep = localtime(&t);
        char * str = ctime(&t);//此字符串后有回车换行及‘/n’,所以要去掉
        char tempbuff[25];
        memset(tempbuff,0,25);
        memcpy(tempbuff,str,24);
        snprintf(buffer,ARRY_SIZE(buffer),"%s%f",tempbuff,temperature);
        printf("%s\n",buffer);
        
/*          if(connect_flag)
        {
            mosquitto_lib_init();
            mosq = mosquitto_new(NULL,true,NULL);

            if(!mosq)
            {
                printf("mosquitto_new() failed: %s\n",strerror(errno));
                mosquitto_destroy(mosq);
                mosquitto_lib_cleanup();
                continue;                
            }

            printf("Create mosquitto successfully!\n");

             if(mosquitto_connect(mosq,ip,port,KEEP_ALIVE) != MOSQ_ERR_SUCCESS)
            {
                printf("mosquitto_connect() failed: %s\n",strerror(errno));

                 mosquitto_destroy(mosq);
                mosquitto_lib_cleanup();
                continue;
            }

            printf("connect %s:%d successfully!\n",ip,port);

            if(mosquitto_loop_start(mosq) != MOSQ_ERR_SUCCESS)
            {
            
                printf("mosquitto_loop_start() failed: %s\n",strerror(errno));
                mosquitto_destroy(mosq);
                mosquitto_lib_cleanup();
                continue;
            }

            connect_flag = 0;
        }
*/

        if(mosquitto_publish(mosq,NULL,topic,strlen(buffer),buffer,0,0) != MOSQ_ERR_SUCCESS)
        {
            
                printf("mosquitto_publish() failed: %s\n",strerror(errno));
                mosquitto_destroy(mosq);
                mosquitto_lib_cleanup();
            
        
        }else
        {
            printf("Publish information of temperature Ok!\n") ;
        }
        sleep(30);
     }

    close(log_fd);
     return 0;
} 

void print_usage(char *progname)
{
    printf("%s usage:\n",progname);
    printf("-p(--port): sepcify server listen port.\n");
    printf("-h(--Help): print this help information.\n");
    printf("-d(--daemon): set program running on background.\n");
    printf("\nExample: %s -d -p 8889\n",progname);
}



