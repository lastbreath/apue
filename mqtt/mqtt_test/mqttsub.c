/*********************************************************************************
 *      Copyright:  (C) 2021 lastbreath<2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  socketserv_epoll.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(08/08/2021)
 *         Author:
 *
 *         lastbreath <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "08/08/2021 05:08:41 PM"
 *                 
 ********************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <mysql.h>
#include "mosquitto.h"
#include <signal.h>
#include <libgen.h>
#include <netdb.h>

#define MSG_MAX_SZIE 1024
#define ARRY_SIZE(x)    (sizeof(x)/sizeof(x[0]))
#define KEEP_ALIVE 60

static inline void print_usage(char *progname);
int dboperate(char *buffer);
int g_stop = 0;

void my_message_calllback(struct mosquitto *mosq,void *obj,const struct mosquitto_message *msg);
void sig_handler(int sig_num)
{
    if(sig_num == SIGUSR1)
        g_stop = 1;
            
}
     

int main (int argc, char **argv)
{
    char *progname = basename(argv[0]);
    char *ip = NULL;
    char *hostname = NULL;
    char *topic = NULL;
    struct hostent *hostentp;
    struct mosquitto *mosq = NULL;
    int connect_flag = 1;
    int log_fd;
    int port;
    int daemon_run = 0;
    int opt;
    char buffer[MSG_MAX_SZIE];
    //char *obj = buffer;


     struct option options[] = 
     {
         {"daemon",no_argument,NULL,'d'},   
         {"port",required_argument,NULL,'p'},
         {"topic", required_argument,NULL,'t'},
         {"hostname", required_argument,NULL,'n'},
         {"ip", required_argument, NULL, 'i'},
         {"help",no_argument,NULL,'h'},
         {NULL,0,NULL,0}
     };

     while((opt = getopt_long(argc,argv,"dhp:t:i:n:",options,NULL)) != -1)
     {
        
         switch(opt)
         
         {
             case 't':
                  topic = optarg;
                  break;
             case 'i':
                  ip = optarg;
                  break;
             case 'n':
                  hostname = optarg;
                  break;
             case 'd':
                 daemon_run = 1;
                 break;
             case 'p':
                 port = atoi(optarg);
                 break;
             case 'h':
                 print_usage(argv[0]);
                 return 0;
             default:
                 break;
         }
     }

     if(!port)
     {
         print_usage(progname);
         return -1;
     }

     if(!hostname && !ip)
     {
         print_usage(progname);
         return -1;
     }
    
    if(hostname)
    {
        hostentp = gethostbyname(hostname);
        if(!hostname)
        {
            printf("Failed to get host by name: %s\n",strerror(errno));
            return -2;
        }
        printf("hostname: %s\n",hostentp->h_name);

        ip = inet_ntoa(*(struct in_addr *)hostentp->h_addr);
        printf("address: %s\n",ip);
    }

    if(!topic)
    {
        topic = "temperature";
    }


     if(daemon_run)
     {
         printf("program %s running in background\n",progname);
         if((log_fd = open("server.log",O_CREAT|O_RDWR,0666)) < 0)
         {
             printf("open() server.log  failed: %s\n",strerror(errno));
             return -2;
         }

         dup2(log_fd,STDOUT_FILENO);
         dup2(log_fd,STDERR_FILENO);
         daemon(1,1);
     }
     

     signal(SIGUSR1,sig_handler);

    mosquitto_lib_init();
    mosq = mosquitto_new(NULL,true,(void *)buffer);
    if(!mosq)
    {
        printf("failed to new mosquitto: %s\n",strerror(errno));
        mosquitto_lib_cleanup();
        return -1;
    }

    printf("new mosq successfully!\n");
    mosquitto_message_callback_set(mosq,my_message_calllback);
    
    if(mosquitto_connect(mosq,ip,port,KEEP_ALIVE) != MOSQ_ERR_SUCCESS)
    {
        printf("mosquitto_connect() failed: %s\n",strerror(errno));
         mosquitto_destroy(mosq);
          mosquitto_lib_cleanup();
          return -2;
    }
    printf("mosq connect successfully!\n");
    if((mosquitto_subscribe(mosq,NULL,topic,2)) != MOSQ_ERR_SUCCESS)
    {
        printf("failed to subscribe from broker: %s\n",strerror(errno));
        mosquitto_destroy(mosq);
        mosquitto_lib_cleanup();
        return -3;
    }
    printf("mosq subscribe successfully!\n");
     while(!g_stop)
     {
         mosquitto_loop(mosq,-1,1);
     }



     mosquitto_destroy(mosq);
     mosquitto_lib_cleanup();
     return 0;
} 

void print_usage(char *progname)
{
    printf("%s usage:\n",progname);
    printf("-p(--port): sepcify server listen port.\n");
    printf("-h(--Help): print this help information.\n");
    printf("-d(--daemon): set program running on background.\n");
    printf("\nExample: %s -d -p 8889\n",progname);
}




int dboperate(char *buffer)
{
	        MYSQL mysql,*sock;	
		MYSQL_RES *res;
		MYSQL_FIELD *fd;
		MYSQL_ROW row;
        char qbuf[1024];

        char str1[25];//字符数组存字符串时 要多一个字节 来存放‘/0’
        memset(str1,0,25);
        memcpy(str1,buffer,24);
        char str2[1024];
        memset(str2,0,1024);
        memcpy(str2,buffer+24,1024-24);
        //printf("%d\n",222);
        printf("%s\n",str1);
        //printf("%d\n",224);
        printf("%s\n",str2);
        float t = atof(str2);

		mysql_init(&mysql);
        
	       	sock = mysql_real_connect(&mysql,"localhost","root","12345678","mydb",0,NULL,0);
		if(!sock)
		{
			fprintf(stderr,"Couldn't connect to database!: %s\n",mysql_error(&mysql));
			exit(1);
		}
        memset(qbuf,0,ARRY_SIZE(qbuf));
		sprintf(qbuf,"insert temperature(subtime,temperature) values('%s',%f)",str1,t);

		if(mysql_query(sock,qbuf))// when mysql_query() excute seccessfully it return 0;

		{
			fprintf(stderr,"Excute failed!: %s\n",mysql_error(sock));
			exit(1);
		}
        printf("write into database successfully!\n");
		return 0;
}


void my_message_calllback(struct mosquitto *mosq,void *obj,const struct mosquitto_message *msg)
{
    memset((char *)obj,0,MSG_MAX_SZIE);
    memcpy((char *)obj,(char *)msg->payload,MSG_MAX_SZIE);
    printf("%s\n",(char *)obj);
    dboperate((char *)obj);


}
