/*********************************************************************************
 *      Copyright:  (C) 2021 lastbreath<2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  iniparser_test.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(10/17/2021)
 *         Author:  lastbreath <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "10/17/2021 11:41:52 AM"
 *                 
 ********************************************************************************/

#include <stdio.h>

#include "iniparser.h"

#include "dictionary.h"

#define PATH "./config.ini"



int main (int argc, char **argv)
{
    FILE *fp = NULL;
    dictionary *inip = NULL;
    inip = iniparser_load(PATH);
    if(!inip)
    {
        printf("iniparser_load failure\n");
        return -1;
    }
    int n = iniparser_getnsec(inip);
    if(n)
    {
        for(int i = 0; i < n; i++)
        {
            printf("%s\n",iniparser_getsecname(inip,i));
            int i1 = iniparser_getsecnkeys(inip,iniparser_getsecname(inip,i));
             char *keys[i1];
            char *str = iniparser_getsecname(inip,i);
             iniparser_getseckeys(inip,str,keys);
            char **ptr = keys;
            for(int j = 0; j < i1; j++)
            
            {
                printf("%s\n",*ptr);
                ptr++;
                
            }
        }
    }
    else
    {
        printf("This file has no section!\n");
        return -1;
    }




    iniparser_set(inip,"ipaddrpool:start","192");
    iniparser_set(inip,"ipaddrpool:end","198");
    iniparser_unset(inip,"opt:dns1");

    FILE *fp1 = fopen(PATH,"w");
    iniparser_dump_ini(inip,fp1);
   iniparser_freedict(inip);

    fclose(fp1);

    return 0;
} 

