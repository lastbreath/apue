/*********************************************************************************
 *      Copyright:  (C) 2021 lastbreath<2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  test.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(10/24/2021)
 *         Author:  lastbreath <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "10/24/2021 05:30:16 PM"
 *                 
 ********************************************************************************/

#include <stdio.h>
#include "cJSON.h"

int main ()
{
 
    cJSON   *root = cJSON_CreateObject();
    cJSON   *item = cJSON_CreateObject();
    
    
    cJSON_AddItemToObject(root,"method",cJSON_CreateString("mqtt->method"));
    cJSON_AddItemToObject(root,"id",cJSON_CreateString("mqtt->jsonid"));
    cJSON_AddItemToObject(root,"params",item);
    cJSON_AddItemToObject(item,"CurrentTemperature",cJSON_CreateNumber(25.000));
    cJSON_AddItemToObject(root,"version",cJSON_CreateString("mqtt->version"));
    
    char *msg = cJSON_Print(root);
    
    printf("%s\n",msg);


    cJSON_Delete(root);
    return 0;
}
