/********************************************************************************
 *      Copyright:  (C) 2021 lastbreath<2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  configoperate.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(10/18/2021)
 *         Author:  lastbreath <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "10/18/2021 06:12:09 PM"
 *                 
 ********************************************************************************/

#ifndef CONFIGOPERATE_H
#define CONFIGOPERATE_H


#define SIZE    1024

#define DEFAULT_CLIENTID    "12345|securemode=3,signmethod=hmacsha1|"
#define DEFAULT_USERNAME    "test&ghcey1O2Yxm"
#define DEFAULT_PASSWORD "D090FBDCC327200DAD3083684A891A1C7992FACB"
#define DEFAULT_BROKER_ADDRESS    "ghcey1O2Yxm.iot-as-mqtt.cn-shanghai.aliyuncs.com"
#define DEFAULT_BROKER_PORT 1883
#define DEFAULT_SUB_TOPIC   "/sys/ghcey1O2Yxm/test/thing/service/property/set"
#define DEFAULT_PUB_TOPIC   "/sys/ghcey1O2Yxm/test/thing/event/property/post"
#define DEFAULT_QOS         0


#define DEFAULT_METHOD  "thing.event.property.post"
#define DEFAULT_JSONID  "0801"
#define DEFAULT_IDENTIFIER  "data"
#define DEFAULT_VERSION "1.0.0"


#define KEEP_ALIVE  60

enum mode
{
    SUB,
    PUB
};
typedef struct mqtt_user_data{

    char    clientid[SIZE];
    char    username[SIZE];
    char    password[SIZE];
    char    brokeraddress[SIZE];
    int     brokerport;
    char    topic[SIZE];
    int     Qos;

    char    method[SIZE];
    char    jsonid[SIZE];
    char    identifier[SIZE];
    char    version[SIZE];
             
}mqtt_user_data;


int set_config(char *ini,char *host,int port, char *clientid,char *uname,char *pwd,char *topic);
int get_config(char *path,mqtt_user_data *mqtt,int mode);


#endif
