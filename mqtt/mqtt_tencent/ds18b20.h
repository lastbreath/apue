#ifndef _DS18_H_
#define _DS18_H_

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>

#define PATH "/sys/bus/w1/devices/"
#define BUF_SIZE 1024


extern int ds18b20_get_temperature(float *t);



#endif
