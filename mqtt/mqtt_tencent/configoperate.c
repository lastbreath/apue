/*********************************************************************************
 *      Copyright:  (C) 2021 lastbreath<2631336290@qq.com>
 *                  All rights reserved.
 *
 *       Filename:  configoperate.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(10/18/2021)
 *         Author:  lastbreath <2631336290@qq.com>
 *      ChangeLog:  1, Release initial version on "10/18/2021 06:11:37 PM"
 *                 
 ********************************************************************************/

#include "configoperate.h"
#include <stdio.h>
#include <string.h>
#include "iniparser.h"

    


int set_config(char *path,char *host,int port, char *clientid,char *uname,char *pwd,char *topic)
{
    FILE    *fp = NULL;
    dictionary  *ini = NULL;
    char    mqtt_port[16];

    if(port <= 0)
    {
        printf("invalid_argument: port!\n");
        return -1;
    }

    memset(mqtt_port,0,sizeof(mqtt_port));
    snprintf(mqtt_port,sizeof(mqtt_port),"%d",port);

    ini = iniparser_load(path);

    if(!ini)
    {
        printf("iniparser_load failed!\n");    
        return -1;
    }

    iniparser_set(ini,"address:host",host);
    iniparser_set(ini,"address:port",mqtt_port);
    iniparser_set(ini,"user_name_pwd:username",uname);
    iniparser_set(ini,"user_name_pwd:pwd",pwd);
    iniparser_set(ini,"clientid:id",clientid);
    iniparser_set(ini,"pub_topic:topic",topic);


    fp = fopen(path,"w");
    if(!fp)
    {
        printf("fopen failed!\n");
        return -1;
    }

    iniparser_dump_ini(ini,fp);

    iniparser_freedict(ini);
    return 0;

}

int get_config(char *path,mqtt_user_data *mqtt,int mode)
{
    dictionary      *ini = NULL;
    const char      *brokeraddress;
    int             brokerport;
    const char      *username;
    const char      *password;
    const char      *clientid;
    const char      *topic;
    int             Qos;

    const char      *method;
    const char      *jsonid;
    const char      *identifier;
    const char      *version;



    if(!path || !mqtt)
    {
        printf("invalid_argument: %s\n",__FUNCTION__);
        return -1;
    }

    ini = iniparser_load(path);

    if(!ini)
    {
        printf("iniparser_load failed!\n");
        return -1;
    }

    brokeraddress = iniparser_getstring(ini,"address:host",DEFAULT_BROKER_ADDRESS);
    brokerport= iniparser_getint(ini,"address:port",DEFAULT_BROKER_PORT);
    username = iniparser_getstring(ini,"user_name_pwd:username",DEFAULT_USERNAME);
    password = iniparser_getstring(ini,"user_name_pwd:pwd",DEFAULT_PASSWORD);
    clientid = iniparser_getstring(ini,"clientid:id",DEFAULT_CLIENTID);
    Qos      = iniparser_getint(ini,"Qos:Qos",DEFAULT_QOS);

    identifier = iniparser_getstring(ini,"json:identifier",DEFAULT_JSONID);

    if(mode == SUB)
    {
        topic = iniparser_getstring(ini,"sub_topic:topic",DEFAULT_SUB_TOPIC);
    }
    else if(mode == PUB)
    {

        topic = iniparser_getstring(ini,"pub_topic:topic",DEFAULT_PUB_TOPIC);
        method = iniparser_getstring(ini,"json:method",DEFAULT_METHOD);
        jsonid = iniparser_getstring(ini,"json:id",DEFAULT_JSONID);
        version = iniparser_getstring(ini,"json:version",DEFAULT_VERSION);
    }
    else if(mode != SUB && mode != PUB)
    {
        printf("invalid_argument : %s mode!\n",__FUNCTION__);
        return -1;
    }

    strncpy(mqtt->brokeraddress,brokeraddress,SIZE);
    mqtt->brokerport = brokerport;
    mqtt->Qos       =Qos;
    strncpy(mqtt->username,username,SIZE);
    strncpy(mqtt->password,password,SIZE);
    strncpy(mqtt->clientid,clientid,SIZE);
    strncpy(mqtt->topic,topic,SIZE);          
    strncpy(mqtt->identifier,identifier,SIZE);
    if(mode == PUB)
    {
        strncpy(mqtt->method,method,SIZE);
        strncpy(mqtt->jsonid,jsonid,SIZE);
        strncpy(mqtt->version,version,SIZE);
    }

    iniparser_freedict(ini);
    return 0;

}

